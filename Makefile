BROWSER = firefox

default:
	@echo "'clean-build'" to clean the build tree.  
	@echo "'clean-pyc'" to clean the project tree of all bytecode files.
	@echo "'clean'"	perform clean-build and clean-pyc.
	@echo "'commit'" perform a clean, requirements-dev, and a git commit -a.
	@echo "'coverage'" perform a pytest coverage report, output to the terminal.
	@echo "'coverage-html'" perform a pytest coverage html report, output to htmlcov.
	@echo "'install'" to do a typical install, with only the necessary deps, to the --user site-packages.
	@echo "'install-dev'" to install all dev deps, then ffconfig with pip -e 
	@echo "'lint'" perform autopep8 and pylint on src/ and test/
	@echo "'requirements-dev'" to perform a pip freeze to requirements-dev.txt.
	@echo "'test'" perform tests on src/.

.PHONY: clean-build
clean-build:
	rm -rf build/
	rm -rf dist/
	rm -rf *.egg-info

.PHONY: clean-pyc
clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	rm -rf htmlcov/*

.PHONY: clean
clean: clean-build clean-pyc

.PHONY: commit
commit: clean requirements-dev
	git commit -a

.PHONY: coverage
coverage:
	pytest --cov=src/ --cov-report=term-missing:skip-covered --cov-branch

.PHONY: coverage-html
coverage-html:
	rm -rf htmlcov/*
	pytest --cov=src/ --cov-report=html:htmlcov --cov-branch
	firefox ./htmlcov/index.html

.PHONY: install
install: clean
	pip install --user ./requirements.txt
	pip install --user ./

.PHONY: install-dev
install-dev: clean
	pip install ./requirements-dev.txt

.PHONY: lint
lint:
	autopep8 -irv --max-line-length 120 src/ test/
	pylint src/ test/

.PHONY: requirements-dev
requirements-dev:
	pip freeze > ./requirements-dev.txt

.PHONY: test
test: clean-pyc
	pytest ./
